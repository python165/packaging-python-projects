ifeq ($(shell uname -s),Darwin)
	OPENSCAD := /Applications/OpenSCAD.app/Contents/MacOS/OpenSCAD
else ifeq ($(shell uname -s),Linux)
	OPENSCAD := /usr/bin/openscad
endif


SHELL := bash

INDENT := 2>&1 | sed 's/^/    /'

VENV := venv.python3

DO_VENV := . ${VENV}/bin/activate &&

HUSH := @


LIST_TARGETS :=  $(MAKE) -pRrq -f $(firstword $(MAKEFILE_LIST)) : 2>/dev/null | awk -v RS= -F: '/^\# Implicit Rules/,/^\# Finished Make data base/ {if ($$1 !~ "^[\#.]") {print $$1}}'


make.targets :
	${HUSH} echo "available Make targets:"
	${HUSH} ${LIST_TARGETS} \
	| egrep -v '^make.targets$$' \
	| sed 's/^/    make    /' \
	| env LC_COLLATE=C sort



diag :
	${HUSH} echo "running on $(shell uname -s)"
	${HUSH} echo "python venv is ${VENV}"


sanity :
	${HUSH} echo "checking ... "
	${HUSH} echo "* for non-system python" && [ $$(which python) = "/usr/bin/python" ] && { echo "! don't use the system python" ; exit 1 ; } || true


readme :
	${HUSH} cat README.md

notes:
	${HUSH} echo ". ${VENV}/bin/activate"


venv : ${VENV} venv.pip.packages

${VENV} : 
	python3 -m venv ${VENV}


venv.clean :
	rm -rf ${VENV}


venv.pip.packages :
	${HUSH} ${DO_VENV} make sanity
	${HUSH} ${DO_VENV} pip install --upgrade pip

	${HUSH} ${DO_VENV} pip install --upgrade \
		build \
		pyinstaller \
		setuptools \
		twine \
		wheel \
		 # 

venv.pip.freeze :
	${DO_VENV} make sanity
	${DO_VENV} pip freeze

python.build :
	${DO_VENV} python -m build

python.clean : 
	[ -d dist ] && rm -rf dist || echo nothing to do
	[ -d src/hello_world_michaeldallen.egg-info ] && rm -rf src/hello_world_michaeldallen.egg-info/ || echo nothing to do
	${HUSH} echo all clean

python : python.build


pypi.upload :
	${DO_VENV} python -m twine upload --repository testpypi dist/* -u __token__


clean : 
	${HUSH} find * -depth -type f -name '*~' -exec rm -v {} \;


clean.wicked :
	${HUSH} ${LIST_TARGETS} | grep '.clean$$' | while read target ; do echo make $${target} ; ${MAKE} $${target} ${INDENT} ; done


all : venv python

#EOF
